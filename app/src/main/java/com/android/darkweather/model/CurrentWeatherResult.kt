package com.android.darkweather.model

data class CurrentWeatherResult (
    var timezone: String,
    var currently: Weather,
    var daily: Daily
)

data class Daily (
    var summary: String,
    var data: List<Weather>
)

data class Weather (
    var time: Long,
    var summary: String,
    var temperatureHigh: Double,
    var temperatureLow: Double,
    var temperature: Double,
    var humidity: Double,
    var pressure: Double,
    var windSpeed: Double,
    var visibility: Double
)