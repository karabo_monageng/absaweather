package com.android.darkweather.api

import com.android.darkweather.util.CommonStrings
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WeatherService {

    fun getCurrentWeather(): WeatherApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(CommonStrings.BASE_WEATHER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(WeatherApi::class.java)
    }
}