package com.android.darkweather.api

import com.android.darkweather.model.CurrentWeatherResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface WeatherApi {

    @GET("forecast/{secret}/{latLon}")
    fun getCurrentWeatherAsync(@Path("secret") secret: String, @Path("latLon") latLon: String): Call<CurrentWeatherResult>

}