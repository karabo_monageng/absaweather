package com.android.darkweather.util

class CommonStrings {
    companion object {
        const val BASE_WEATHER_URL = "https://api.darksky.net/"
        const val API_SECRET = "c501f61d7dcceb58289b4cd3dd2cf445"
        const val DEGREES_SYMBOL = (0x00B0).toChar()
    }
}