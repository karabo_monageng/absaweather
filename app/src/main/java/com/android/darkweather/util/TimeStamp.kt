package com.android.darkweather.util

import java.text.SimpleDateFormat
import java.util.*

class TimeStamp {
    companion object {
        fun getTime(epoch: Long) : String {
//            val formatter = SimpleDateFormat("dd/MM/yyyy")
//            val dateString = formatter.format(Date(java.lang.Long.parseLong(time.getText().toString())))

            val dateFormat = SimpleDateFormat("MMM dd yyyy HH:mm", Locale.getDefault())

            return dateFormat.format(Date(epoch))
        }
    }
}