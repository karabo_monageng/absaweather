package com.android.darkweather.ui.main

import android.annotation.SuppressLint
import android.content.DialogInterface
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.darkweather.R
import com.android.darkweather.model.Weather
import com.android.darkweather.ui.forecast.ForecastItemClickListener
import com.android.darkweather.ui.forecast.ForecastListAdapter
import com.android.darkweather.util.CommonStrings
import com.android.darkweather.util.TimeStamp
import kotlinx.android.synthetic.main.content_current_weather.*
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment(), ForecastItemClickListener {
    private val TAG = javaClass.name

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var forecastAdapter: ForecastListAdapter

    override fun onItemClick(weather: Weather) {
        Toast.makeText(context, "Loading bottom sheet", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

//        viewModel = MainViewModel()

        viewModel.isLoading.observe(this, Observer { isLoading ->
            mainProgressBar.visibility = if(isLoading) {
                View.VISIBLE
            } else {
                View.GONE
            }
        })

        viewModel.showError.observe(this, Observer { errorMessage ->
            AlertDialog.Builder(context!!)
                .setTitle("Error")
                .setMessage(errorMessage)
                .setCancelable(false)
                .setPositiveButton("Ok") { dialogInterface: DialogInterface, _: Int ->
                    dialogInterface.dismiss()
                }
                .create()
                .show()
        })

        viewModel.currentWeatherResult.observe(this, Observer { result ->
            textTimezone.text = "TimeZone: ${result.timezone}"

            val currentWeather = result?.currently!!

            textTimestamp.text = "Time: ${TimeStamp.getTime(currentWeather.time)}"
//            textTimestamp.text = "Time: ${currentWeather.time}"
            textSummary.text = "Summary: ${currentWeather.summary}"
            textTemperature.text = "${currentWeather.temperature} ${CommonStrings.DEGREES_SYMBOL}"
            textHumidity.text = "Humidity: ${currentWeather.humidity}"
            textPressure.text = "Pressure: ${currentWeather.pressure}"
            textWindSpeed.text = "Wind Speed: ${currentWeather.windSpeed}"
            textVisibility.text = "Visibility: ${currentWeather.visibility}"

            displayForecastList(result.daily.data)

        })

        viewModel.getCurrentWeather(-25.9886399,27.9540198)

    }

    private fun displayForecastList(weatherForecast: List<Weather>) {
        forecastAdapter = ForecastListAdapter(this)
        forecastAdapter.updateData(weatherForecast)

        recyclerViewForecast.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = forecastAdapter
        }
    }
}
