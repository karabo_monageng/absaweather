package com.android.darkweather.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.darkweather.api.WeatherService
import com.android.darkweather.model.CurrentWeatherResult
import com.android.darkweather.util.UseCaseResult
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MainViewModel : ViewModel(), CoroutineScope {
    private val repository = CurrentWeatherRepository(api = WeatherService().getCurrentWeather())
    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    val isLoading = MutableLiveData<Boolean>()
    val currentWeatherResult = MutableLiveData<CurrentWeatherResult>()
    val showError = MutableLiveData<String>()

    fun getCurrentWeather(lat: Double, lon: Double) {
        isLoading.value = true

        launch {
            val result = withContext(Dispatchers.IO) {
                repository.getCurrentWeather(lat, lon)
            }

            isLoading.value = false

            when(result) {
                is UseCaseResult.Success -> currentWeatherResult.value = result.data
                is UseCaseResult.Error -> showError.value = result.exception.message
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}
