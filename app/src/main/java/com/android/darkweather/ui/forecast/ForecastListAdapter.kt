package com.android.darkweather.ui.forecast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.darkweather.R
import com.android.darkweather.model.Weather
import com.android.darkweather.util.CommonStrings
import kotlinx.android.synthetic.main.list_item_forecast.view.*

class ForecastListAdapter(private val itemClickListener: ForecastItemClickListener) : RecyclerView.Adapter<ForecastViewHolder>() {
    private var forecastList: List<Weather> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_forecast, parent, false)

        return ForecastViewHolder(view)
    }

    override fun getItemCount(): Int = forecastList.size


    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        if(position != RecyclerView.NO_POSITION) {
            val weather: Weather = forecastList[position]
            holder.bind(weather, itemClickListener)
        }
    }

    fun updateData(forecastList: List<Weather>) {
        this.forecastList = forecastList
    }
}

class ForecastViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(weather: Weather, clickListener: ForecastItemClickListener) {
        itemView.textListTime.text = "Time: ${weather.time}"
        itemView.textListSummary.text = "Summary: ${weather.summary}"
        itemView.textListTemperature.text = "${weather.temperatureHigh}${CommonStrings.DEGREES_SYMBOL}\n${weather.temperatureLow}${CommonStrings.DEGREES_SYMBOL}"

        itemView.setOnClickListener {
            clickListener.onItemClick(weather)
        }
    }
}

interface ForecastItemClickListener {
    fun onItemClick(weather: Weather)
}