package com.android.darkweather.ui.main

import com.android.darkweather.api.WeatherApi
import com.android.darkweather.model.CurrentWeatherResult
import com.android.darkweather.util.CommonStrings
import com.android.darkweather.util.UseCaseResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.await
import kotlin.coroutines.CoroutineContext

// TODO: Include API in constructor call
class CurrentWeatherRepository(private val api: WeatherApi) : CoroutineScope {
    override val coroutineContext: CoroutineContext = Dispatchers.Main
//    private val api = WeatherService().getCurrentWeather()

    suspend fun getCurrentWeather(lat: Double, lon: Double): UseCaseResult<CurrentWeatherResult> {
        return try {
            val result = api.getCurrentWeatherAsync(CommonStrings.API_SECRET,"$lat,$lon").await()

            UseCaseResult.Success(result)
        } catch (ex: Exception) {
            UseCaseResult.Error(ex)
        }
    }
}